+++
title = "Home"
outputs = ["Reveal"]
[logo]
src = "Logo-MAX-couleur.png"
alt = "Max logo"
margin = 0.2
+++

{{<slide background-image="background.png" background-opacity="0.1" background-position="0% 0%">}}

## Documentation as code can't be suspicious 📝

<br/>
<br/>

<div>
    <img style="border-radius:50%; width:3em;" src="mat_souch.jpg">
    <div style="display:flex; justify-content:center; align-items:center;">
        <img src="twitter-icon.png" style="border:none; background:transparent; margin-right:15px;" /><a href="https://twitter.com/Gasouch">@Gasouch</a>
    </div>
</div>

{{% note %}}
* Bonjour, aujourd'hui je vais vous raconter une histoire triste ... mais qui fini pleine d'espoir
{{% /note %}}

---

{{<slide background-image="background.png" background-opacity="0.1" background-position="0% 0%">}}

{{% section %}}

## Il était une fois

{{% fragment %}}<img src="enterprise.jpg" style="width: 600px"/>{{% /fragment %}}

{{% note %}}
* Il était une fois, un équipage à la pointe des méthodes de travail, ils étaient agiles, ils faisaient du SAFE ... ouaa ... et ça marchait ! ... ouaaa
* Ils avaient des supers outils, pertinents pour leur usage, et étaient super efficaces dans leur boulot
* C'était l'équipage du vaisseau ... Click ... Good Practises Entreprise !
* Mais par une journée qui pourtant semblait anodine, un drame arriva
* L'équipage était en atelier de conception, ils avaient presque fini, quand tout à coup...
{{% /note %}}

---

![drame](drame.png)

{{% note %}}
* Et comme nous sommes dans le monde impitoyable d'Among Us ...
{{% /note %}}

---

<img src="rose-impostor-ejected.gif" style="border:none; width:800px;"/>

---

## Cycle de vie de la doc

{{% fragment %}}1 : Rédaction{{% /fragment %}}
{{% fragment %}}2 : Collaboration{{% /fragment %}}
{{% fragment %}}3 : Publication{{% /fragment %}}
<div style="display:flex; justify-content:center; align-items:center;">
{{% fragment %}}
<img src="word.png" style="border:none; background:transparent; width:100px; margin-right:20px;"/>
<img src="outlook.png" style="border:none; background:transparent; width:100px; margin-left:20px;"/>
{{% /fragment %}}
</div>

{{% note %}}
* Réfléchissons à ce qui vient de se passer : Rose s'est fait sanctionné pour son traitement de la doc
* Click / Rédaction dans Word (ou sharepoint)
* Tentatives de mise en place de sémantique via du style (italique / gras / rouge / taille de police)
* Versionnage par nom de fichier ou par annotations dans le document
* Collaboration par email ... click click
{{% /note %}}

---

## Mhh n'y a t-il pas mieux à faire ?

![thinking](thinking.png)

{{% note %}}
* J'imagine que vous avez bien des suggestions d'amélioration qui vous viennent en tête? 😁
* Rédiger / Collaborer / Publier ... En fait, pour les documentations ou autres rédactions diverses (blog, support de formation, livres ...) -> besoins similaires au code applicatif
* Cool, les outils que les Devs utilisent pour ces 3 étapes existent déjà
* ça y est, on y vient ...
{{% /note %}}

---

## Document as Code ? 📖

{{% fragment %}}doc = code{{% /fragment %}}

{{% note %}}
* Késako : L'idée générale, c'est de traiter la documentation comme du code
* OK donc la doc c'est du code en fait ?
* click / Ben ouais pourquoi pas ?
{{% /note %}}

{{% /section %}}

---

{{<slide background-image="background.png" background-opacity="0.1" background-position="0% 0%">}}

{{% section %}}

## 1 : Rédaction

{{% fragment %}}Separation Of Concerns (Présentation != Contenu){{% /fragment %}}
{{% fragment %}}Priorité = contenu{{% /fragment %}}
{{% fragment %}}Simplifier le chemin cerveau ➡ machine{{% /fragment %}}

{{% note %}}
* Click : Afficher Separation Of Concerns
* On a des contraintes lorsqu'on rédige de la doc, des notes
  - ex1 : Compte Rendu de réunion -> rapidité/efficacité
  - ex2 : Rédaction d'un raisonnement complexe -> on doit rester focus sur le fil conducteur de nos idées
* L'idéal donc est de libérer le rédacteur de tout ce qui pourrait le parasiter, le freiner (-> la présentation), pour qu'il puisse se concentrer uniquement sur le contenu (Priorité = contenu). Simplifier le chemin cerveau ➡ machine
* Click + Click
{{% /note %}}

---

{{% fragment %}}Le WYSIWYG ça peut être pratique ... mais en fait ça peut être un source d'emmerdements{{% /fragment %}}
{{% fragment %}}➡ Notepad{{% /fragment %}}
{{% fragment %}}➡ Markdown / AsciiDoc{{% /fragment %}}
{{% fragment %}}Pour une prise de note rapide à rédiger et partager : HackMD{{% /fragment %}}
{{% fragment %}}Templatiser les modèles de docs{{% /fragment %}}
{{% fragment %}}DRY : Don't Repeat Yourself{{% /fragment %}}
{{% fragment %}}Bien choisir son IDE{{% /fragment %}}

{{% note %}}
* WYSIWYG : des comportements incompréhensibles, clickodrome, limitations pour la collaboration (génération de markup)
* Certains outils (HackMD, Gitlab) proposent des templates pour différents types de documentation
* AsciiDoc permet d'inclure d'autres documents AsciiDoc, il autorise l'utilisation de variables
{{% /note %}}

---

### HackMD

<div style="display:flex; justify-content:center; align-items:center;">
    <img src="hackmd-1.png" style="border:none; background:transparent; width:600px; margin-right:20px;"/><img src="hackmd-2.png" style="border:none; background:transparent; width:600px; margin-left:20px;"/>
</div>

---

### AsciDoc

* Depuis 2002
* Le contenu avant la forme
* Se veut lisible naturellement
* Manipuler le texte comme on manipule le code

---

### Demo time

{{% note %}}
* Démo Asccidoc : afficher demo.adoc sur grand écran, puis coder avec preview (mettre la soluce sur écran secondaire)
{{% /note %}}

---

### Liens utiles

* https://asciidoctor.org/docs/asciidoc-recommended-practices
* https://docs.asciidoctor.org/asciidoc/latest
* https://asciidoclive.com

{{% /section %}}

---

{{<slide background-image="background.png" background-opacity="0.1" background-position="0% 0%">}}

{{% section %}}

## 2 : Collaboration

![](collaboration.png)

---

{{% fragment %}}Challenge : Apprendre Git aux non Dev{{% /fragment %}}
{{% fragment %}}Git ♥ Asciidoc{{% /fragment %}}
{{% fragment %}}Branches de travail{{% /fragment %}}
{{% fragment %}}Garder la documentation cohérente{{% /fragment %}}
{{% fragment %}}Lead Editor{{% /fragment %}}
{{% fragment %}}Mettre en place des bonnes pratiques{{% /fragment %}}

{{% note %}}
* Important de collaborer, comme pour le code
* click / Git est fait pour la collaboration autour de contenus/documents
* click / Imaginons le cas d'un blog -> relecture par des pairs pendant que d'autres rédigent également des articles : Git pratique (branches)
* click / Il faut de la cohérence dans la doc (style identique du début à la fin et pas différent en fonction du rédacteur - exemple tutoiement/vouvoiment
* click / De la même manière qu'on a un Lead Developper, on pourrait imaginer un Lead Editor, garant de cette cohérence)
{{% /note %}}

---

### Quelques bonnes pratiques

* Une phrase par ligne
* Issue tracker
* Utiliser les branches de travail
* <div style="display:flex; align-items:center;">Revue de contenu via <img src="gitlab-logo.png" style="border:none; background:transparent; width:60px"/> (MR) /<img src="github-logo.png" style="border:none; background:transparent; width:60px"/> (PR) </div>

{{% note %}}
* Une phrase par ligne : simplifie la relecture, le refactoring, les diffs
* Issue tracker : pour référencer les coquilles, les formulations malheureuses, mauvaise traduct.
{{% /note %}}

---

<img src="gitlab-mr-screen.png" style="border:none; background:transparent; width:80%"/>

{{% /section %}}

---

{{<slide background-image="background.png" background-opacity="0.1" background-position="0% 0%">}}

{{% section %}}

## 3 : Publication

![](publication.png)

{{% note %}}
* Exporter (html, pdf, slides) / Publier
* Asciidoctor
* CICD
{{% /note %}}

---

### Asciidoctor

<img src="asciidoctor-logo.png" style="border:none; background:transparent; width:100px" />

AsciiDoc = syntaxe

Asciidoctor = processeur

{{% fragment %}}<img src="pdf-logo.svg" style="border:none; background:transparent; height:70px" /><img src="html-logo.png" style="border:none; background:transparent; height:70px; margin-left:150px; margin-right:150px;"/><img src="reveal-logo.svg" style="border:none; background:transparent; height:70px;"/>{{% /fragment %}}

{{% note %}}
* Asciidoctor mange de l'AsciiDoc et génère un AST (Abstract Syntax Tree) et depuis cet AST, via différents plugins, il va pouvoir générer différents formats de sortie
* Des plugins sont dispos dans les IDE (montrer exemple export)
* Asciidoc offre plus encore : extensions à ajouter au langage, plugins de build...
{{% /note %}}

---

### Quelques bonnes pratiques

<br/>

{{% fragment %}}Merger pour publier{{% /fragment %}}
{{% fragment %}}Intégration continue{{% /fragment %}}
{{% fragment %}}Déploiement continu{{% /fragment %}}

{{% note %}}
* Blog Max DS
{{% /note %}}

---

<div style="display:flex; justify-content:center; align-items:center;">
    <img src="blog-gitlabci-file.png" style="border:none; background:transparent; width:600px; margin-right:20px;"/><img src="blog-cicd-screen.png" style="border:none; background:transparent; width:600px; margin-left:20px;"/>
</div>

{{% /section %}}

---

{{<slide background-image="background.png" background-opacity="0.1" background-position="0% 0%">}}

## A vous de jouer

![](letsgo.jpg)

{{% note %}}
* Ok, pour des gens totalement en dehors de notre monde tech, c'est chaud (nouvelle approche, nouveaux outils)
* Mais si déjà, on arrive à convaincre les gens de notre monde de laisser tomber le combo Word+Mail, VOUS !
* Vous avez déjà tous les outils présentés ici sous la main (éditeur de code, gestionnaire de version, outil collaboratif autour du gestionnaire de version)
* Il ne vous reste plus qu'à apprivoiser AsciiDoc (voire Asciidoctor) pour traiter votre documentation avec la même passion. Merci
{{% /note %}}
